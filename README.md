# All The cars

Here you can add your favourite type of car to the project

## Getting Started

To start contributing to this project you should fork it to your own GitLab repository. Then you can clone it to your local repository on your machine

### Prerequisites

You will need to have Node.js on your machine and this document will assume that you have yarn (npm should be ok too). To install yarn do:


```
npm install -g yarn
```

you will also need to have jest (that is the testing framework: https://jestjs.io/docs/en/getting-started). To install it just run the yarn command in the terminal in your project directory with no arguments like so:

```
yarn

```

## Contributing

If you want to add something to the project you will have to edit the scripts.js file. To add a car to the project you can add the text to the someCars function like so:
```
var someCars = function(){
    return '<div class="car"><p>This is a Mazda</p> <img src="https://cdntdreditorials.azureedge.net/cache/6/0/1/b/2/7/601b27faa1e58f3e74487b97cf065793e1997551.jpg"></div>'+
    '<div class="car"><p>This is a Chevrolet</p><img src="https://www.gannett-cdn.com/presto/2018/11/29/PDTF/335ebcb2-5928-43b4-a918-6eff491087ab-ChevySilveradoLT01.JPG?crop=2599,1462,x1,y241&width=3200&height=1680&fit=bounds"></div>'+
    '<div class="car"><p>This is a ......</p> <img src="add image"></div>'

}
```
Just remember to add a plus after the previous line like so:

```
var someCars = function(){
    return '<div class="car"><p>This is a Mazda</p> <img src="https://cdntdreditorials.azureedge.net/cache/6/0/1/b/2/7/601b27faa1e58f3e74487b97cf065793e1997551.jpg"></div>'+
    '<div class="car"><p>This is a Chevrolet</p><img src="https://www.gannett-cdn.com/presto/2018/11/29/PDTF/335ebcb2-5928-43b4-a918-6eff491087ab-ChevySilveradoLT01.JPG?crop=2599,1462,x1,y241&width=3200&height=1680&fit=bounds"></div>'+
    '<div class="car"><p>This is a ......</p> <img src="add image"></div>'+
'<div class="car"><p>This is a ......</p> <img src="add image"></div>'

}
```

## Running the tests

Before you commit your code and do a merge request you should test your code by typing:

```
yarn test
```

into your terminal. If everything is successful you should see something like:

```
Test Suites: 3 passed, 3 total
Tests:       6 passed, 6 total
Snapshots:   0 total
Time:        1.089s
Ran all test suites.
Done in 1.76s.
```



## Deployment

Push the repository to gitlab




## Authors

Anton Örn Kærnested

